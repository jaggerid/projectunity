﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerUp : MonoBehaviour
{
    public WaitForSecond wait;
    [SerializeField] private Bird bird;
    [SerializeField] private float speed = 1;
    void Update()
    {
        //Melakukan pengecekan burung mati atau tidak
        if (!bird.IsDead())
        {
            //menggerakan game object kesebelah kiri dengan kecepatan tertentu
            transform.Translate(Vector3.left * speed * Time.deltaTime, Space.World);
        }
    }



    void OnTriggerEnter2D(Collider2D collision)
    {
        //Mendapatkan komponen Bird
        Bird bird = collision.gameObject.GetComponent<Bird>();
        //Menambahkan score jika burung tidak null dan burung belum mati;
        if (bird && !bird.IsDead())
        {
            Destroy(this.gameObject);

            collision.gameObject.GetComponent<Collider2D>().enabled = false;
            SpriteRenderer color = collision.gameObject.GetComponent<SpriteRenderer>();
            color.color = new Color(1f, 1f, 1f, 0.5f);
            //StartCoroutine(returnBack(1, collision));
        }
    }

    IEnumerator returnBack(float seconds, Collider2D collision)
    {
        Debug.Log("sec");
        yield return new WaitForSeconds(seconds);
        Debug.Log(seconds);
        collision.gameObject.GetComponent<Collider2D>().enabled = true;
        SpriteRenderer color = collision.gameObject.GetComponent<SpriteRenderer>();
        color.color = new Color(1f, 1f, 1f, 1f);
    }
}
