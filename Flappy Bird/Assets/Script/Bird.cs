﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;


public class Bird : MonoBehaviour
{
	//Global Variables
	[SerializeField] private float upForce = 100;
	[SerializeField] private bool isDead;
	[SerializeField] private UnityEvent OnJump,OnDead;
    [SerializeField] private int score;
    [SerializeField] private UnityEvent OnAddPoint;
    [SerializeField] private Text scoreText;
    private Animator animator;
	private Rigidbody2D rigidBody2d;
    float timer;

    //init variable
    void Start()
	{
		//Mendapatkan komponent ketika game baru berjalan
        rigidBody2d = GetComponent<Rigidbody2D>();
		//Mendapatkan komponen animator pada game object   
		animator = GetComponent<Animator>();	
	}

    //fungsi score
    public void AddScore(int value)
    {
        //Menambahkan Score value
        score += value;

        //Pengecekan Null Value
        if (OnAddPoint != null)
        {
            //Memanggil semua event pada OnAddPoint
            OnAddPoint.Invoke();
        }

        //Mengubah nilai text pada score text
        scoreText.text = score.ToString();
    }

    //Fungsi untuk mengecek sudah mati atau belum
    public bool IsDead()
    {
        return isDead;
    }

   //Membuat burung mati
    public void Dead()
	{
		//Pengecekan jika belum mati dan value OnDead tidak sama dengan Null
		if (!isDead && OnDead != null)
		{
			//Memanggil semua event pada OnDead
			OnDead.Invoke();
		}
		
		//Mengeset variable Dead menjadi True
		isDead = true;
	}

    void Jump()
	{
		//Mengecek rigidbody null atau tidak
		if (rigidBody2d)
		{
			//menghentikan kecepatan burung ketika jatuh
			rigidBody2d.velocity = Vector2.zero;
			
			//Menambahkan gaya ke arah sumbu y agar burung meloncat
			rigidBody2d.AddForce(new Vector2(0, upForce));
		}
		
		//Pengecekan Null variable
        if (OnJump != null)
        {  
            //Menjalankan semua event OnJump event
            OnJump.Invoke();
        }
	}
	
	private void OnCollisionEnter2D(Collision2D collision)    
   {
        //menghentikan Animasi Burung ketika bersentukan dengan object lain
        Dead();
        animator.enabled = false;
   }

    //Update setiap frame
    void Update()
    {
        //Debug.Log("isDead = " + isDead);
        //Melakukan pengecekan jika belum mati dan klik kiri pada mouse
        if (!isDead && (Input.GetMouseButtonDown(0) || Input.GetKeyDown("space")))
        {
            //Burung meloncat
            Jump();
        }
        if (!isDead && this.GetComponent<Collider2D>() != null)
        {
            if(this.GetComponent<Collider2D>().enabled == false)
            {
                timer += Time.deltaTime;
                if (timer > 5f)
                {
                    AddScore(5);
                    gameObject.GetComponent<Collider2D>().enabled = true;
                    SpriteRenderer color = gameObject.GetComponent<SpriteRenderer>();
                    color.color = new Color(1f, 1f, 1f, 1f);
                    timer = 0;
                }
            }
        }
    }
}
