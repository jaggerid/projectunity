﻿using UnityEngine;
using UnityEngine.Events;

public class Obstacle : MonoBehaviour
{
    public float Health = 50f;

    void OnCollisionEnter2D(Collision2D col)
    {
        if (col.gameObject.GetComponent<Rigidbody2D>() == null) return;

        if (col.gameObject.tag == "Bird")
        {
            float damage = col.gameObject.GetComponent<Rigidbody2D>().velocity.magnitude * 5;
            Health -= damage;
            if (Health <= 0)
            {
                Destroy(gameObject);
            }
        }
        else if (col.gameObject.tag == "Obstacle")
        {
            //Hitung damage yang diperoleh
            float damage = col.gameObject.GetComponent<Rigidbody2D>().velocity.magnitude * 5;
            Health -= damage;
            if (Health <= 0)
            {
                Destroy(gameObject);
            }
        }
    }
}
