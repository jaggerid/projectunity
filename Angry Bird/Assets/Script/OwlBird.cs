﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OwlBird : Bird
{
    [SerializeField]
    private PointEffector2D PointEffector2D;

    private void Start()
    {
        PointEffector2D = GetComponent<PointEffector2D>();
    }
    public void Explode()
    {
        GetComponent<CircleCollider2D>().radius = 2;
        PointEffector2D.enabled = true;
    }
    public void Destroy()
    {
        Destroy(gameObject);
    }
    void OnCollisionEnter2D(Collision2D collider)
    {
        if (collider.gameObject.tag != "Bird")
        GetComponent<SpriteRenderer>().color = Color.red;
        Invoke("Explode", 1);
        Invoke("Destroy", 2);
        //Explode();
    }
}
