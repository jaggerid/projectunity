﻿using UnityEngine.UI;
using UnityEngine;
using UnityEngine.SceneManagement;
using System;

public class GameManager : MonoBehaviour
{
    //Instance sebagai global access
    public DateTime currentTime;
    public TimeSpan timeUntilFinish = TimeSpan.FromMinutes(1);
    public static GameManager instance;
    public Text scoreText;
    public Text timeText;
    public Grid grid;
    public GameObject resultPanel;
    public GameObject resultText;

    // singleton
    void Start()
    {
        currentTime = DateTime.Now;
        grid = GameObject.FindObjectOfType<Grid>();
        if (instance == null)
        {
            instance = this;
        }
        else if (instance != null)
        {
            Destroy(gameObject);
        }

        //DontDestroyOnLoad(gameObject);
    }

    private void Update()
    {
        timeUntilFinish -= DateTime.Now - currentTime;
        currentTime = DateTime.Now;
        timeText.GetComponent<Text>().text = String.Format("Time\nRemaining\n{0:00}:{1:00}", timeUntilFinish.Minutes, timeUntilFinish.Seconds);
        if (timeUntilFinish <= TimeSpan.Zero)
        {
            resultPanel.SetActive(true);
            timeText.GetComponent<Text>().text = "Time\nRemaining\n00:00";
            resultText.GetComponent<Text>().text = "Your Score\n\n" + grid.point;
        }

        scoreText.text = grid.point.ToString();
        //GetScore(grid.point);
    }
    //Update score dan ui
    public void GetScore(int point)
    {
        scoreText.text = point.ToString();
    }
    public void Restart()
    {
        SceneManager.LoadScene(0);
    }
}