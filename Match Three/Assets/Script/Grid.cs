﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Grid : MonoBehaviour
{
    //Properties untuk ukuran grid
    public int round, scoreReq;
    public int gridSizeX, gridSizeY, index, point;
    public Vector2 offset;
    public Vector2 startPosition;
    //Prefab yang akan digunakan untuk background grid
    public GameObject tilePrefab;
    public GameObject candyParent;
    public GameObject powerUpObject;
    public GameObject[] candies;
    public GameObject[,] tiles;


    void Start()
    {

        point = 0;
        tiles = new GameObject[gridSizeX, gridSizeY];
        powerUpObject = GameObject.FindWithTag("PowerUp");
        //Menentukan offset, didapatkan dari size prefab
        offset = tilePrefab.GetComponent<SpriteRenderer>().bounds.size;
        //Menentukan posisi awal
        startPosition = transform.position + (Vector3.left * (offset.x * gridSizeX / 2)) + (Vector3.down * (offset.y * gridSizeY / 3));
        
        CreateGrid();
    }
    
    void CreateGrid()
    {        
        //Looping untuk membuat tile
        for (int x = 0; x < gridSizeX; x++)
        {
            for (int y = 0; y < gridSizeY; y++)
            {
                Vector2 pos = new Vector3(startPosition.x + (x * offset.x), startPosition.y + (y * offset.y));
                GameObject backgroundTile = Instantiate(tilePrefab, pos, tilePrefab.transform.rotation);
                backgroundTile.transform.parent = transform;
                backgroundTile.name = "(" + x + "," + y + ")";

                index = Random.Range(0, candies.Length);

                int MAX_ITERATION = 0;
                while (MatchesAt(x, y, candies[index]) && MAX_ITERATION < 100)
                {
                    index = Random.Range(0, candies.Length);
                    MAX_ITERATION++;
                }

                //GameObject candy = Instantiate(candies[index], pos, Quaternion.identity);
                GameObject candy = ObjectPooler.Instance.SpawnFromPool(index.ToString(), pos, Quaternion.identity);
                //candy.transform.parent = candyParent.transform;
                //candy.name = "(" + x + "," + y + ")";
                tiles[x, y] = candy;

            }
        }
    }
    private bool MatchesAt(int column, int row, GameObject piece)
    {
        //Cek jika ada tile yang sama dengan dibawah dan samping nya
        if (column > 1 && row > 1)
        {
            if (tiles[column - 1, row].tag == piece.tag && tiles[column - 2, row].tag == piece.tag)
            {
                return true;
            }
            if (tiles[column, row - 1].tag == piece.tag && tiles[column, row - 2].tag == piece.tag)
            {
                return true;
            }
        }
        else if (column <= 1 || row <= 1)
        {
            //Cek jika ada tile yang sama dengan atas dan sampingnya
            if (row > 1)
            {
                if (tiles[column, row - 1].tag == piece.tag && tiles[column, row - 2].tag == piece.tag)
                {
                    return true;
                }
            }
            if (column > 1)
            {
                if (tiles[column - 1, row].tag == piece.tag && tiles[column - 2, row].tag == piece.tag)
                {
                    return true;
                }
            }
        }
        return false;
    }
    private void DestroyMatchesAt(int column, int row, bool usePower = false)
    {

        //Destroy tile di indeks tertentu
        if (tiles[column, row].GetComponent<Tile>().isMatched)
        {
            //Destroy(tiles[column, row]);
            GameObject gm = tiles[column, row];
            gm.SetActive(false);
            tiles[column, row] = null;
            point += 10;
        }
        else if (usePower)
        {
            //Destroy(tiles[column, row]);
            GameObject gm = tiles[column, row];
            gm.SetActive(false);
            tiles[column, row] = null;
            point += 10;
        }
    }
    public void DestroyMatches(bool usePower = false, string tilesTag = "none")
    {
        //Lakukan looping untuk cek tile yang null lalu di Destroy
        for (int i = 0; i < gridSizeX; i++)
        {
            for (int j = 0; j < gridSizeY; j++)
            {
                if (usePower)
                {
                    if (tiles[i, j].tag == tilesTag)
                    {
                        DestroyMatchesAt(i, j, true);
                    }
                }
                else if (tiles[i, j] != null)
                {
                    DestroyMatchesAt(i, j);
                }
            }
        }
        StartCoroutine(DecreaseRow());
    }
    private void RefillBoard()
    {
        //Lakukan looping untuk mengecek jika ada tile yang kosong, isi lagi
        for (int i = 0; i < gridSizeX; i++)
        {
            for (int j = 0; j < gridSizeY; j++)
            {
                if (tiles[i, j] == null)
                {
                    Vector2 pos = new Vector3(startPosition.x + (i * offset.x), startPosition.y + (j * offset.y));
                    int index = Random.Range(0, candies.Length);
                    GameObject candy = Instantiate(candies[index], pos, Quaternion.identity);
                    //GameObject candy = ObjectPooler.Instance.SpawnFromPool(index.ToString(), pos, Quaternion.identity);
                    //candy.transform.parent = candyParent.transform;
                    candy.name = "(" + i + "," + j + ")";
                    tiles[i, j] = candy;
                }
            }
        }
    }

    //.....//

    private bool MatchesOnBoard()
    {
        //Check jika tile yang diisi ada yang matching
        for (int i = 0; i < gridSizeX; i++)
        {
            for (int j = 0; j < gridSizeY; j++)
            {
                if (tiles[i, j] != null)
                {
                    if (tiles[i, j].GetComponent<Tile>().isMatched)
                    {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    private IEnumerator DecreaseRow()
    {
        //Lakukan pengurangan row
        int nullCount = 0;
        for (int i = 0; i < gridSizeX; i++)
        {
            for (int j = 0; j < gridSizeY; j++)
            {
                if (tiles[i, j] == null)
                {
                    nullCount++;
                }
                else if (nullCount > 0)
                {
                    tiles[i, j].GetComponent<Tile>().row -= nullCount;
                    tiles[i, j] = null;
                }
            }
            nullCount = 0;
        }
        yield return new WaitForSeconds(.4f);
        StartCoroutine(FillBoard());
    }

    private IEnumerator FillBoard()
    {
        //Isi board kembali
        RefillBoard();
        yield return new WaitForSeconds(.5f);
        while (MatchesOnBoard())
        {
            yield return new WaitForSeconds(.5f);
            DestroyMatches();
        }
    }

    public void resetPoint()
    {
        point = 0;
    }
}