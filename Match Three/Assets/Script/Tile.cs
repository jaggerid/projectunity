﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tile : MonoBehaviour
{
    public bool powerUp = false;
    private Vector3 firstPosition;
    private Vector3 finalPosition;
    private Vector3 tempPosition;
    private float swipeAngle;
    public float xPosition, yPosition;
    public int column, row;
    private int previousColumn, previousRow;
    private Grid grid;
    private GameObject otherTile;
    public bool isMatched = false;

    private void Start()
    {
        float chance = Random.Range(1f, 100f);
        if (chance <= 2.5f)
        {
            powerUp = true;
            SpriteRenderer tempsprite = GetComponent<SpriteRenderer>();
            tempsprite.color = Color.green;
        }
        //Menentukan posisi dari tile
        grid = FindObjectOfType<Grid>();
        xPosition = transform.position.x;
        yPosition = transform.position.y;
        column = Mathf.RoundToInt((xPosition - grid.startPosition.x) / grid.offset.x);
        row = Mathf.RoundToInt((yPosition - grid.startPosition.y) / grid.offset.y);

        previousColumn = column;
        previousRow = row;
    }
    void Update()
    {
        xPosition = (column * grid.offset.x) + grid.startPosition.x;
        yPosition = (row * grid.offset.y) + grid.startPosition.y;
        SwipeTile();

        CheckMatches();
    }
    void OnMouseDown()
    {
        //Mendapatkan titik awal sentuhan jari
        firstPosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
    }
    void OnMouseUp()
    {
        //Mendapatkan titik akhir sentuhan jari
        finalPosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        CalculateAngle();
    }
    void CalculateAngle()
    {
        //Menghitung sudut antara posisi awal dan posisi akhir
        swipeAngle = Mathf.Atan2(finalPosition.y - firstPosition.y, finalPosition.x - firstPosition.x) * 180 / Mathf.PI;
        MoveTile();
    }
    void MoveTile()
    {
        if (swipeAngle > -45 && swipeAngle <= 45)
        {
            //Right swipe
            SwipeRightMove();
        }
        else if (swipeAngle > 45 && swipeAngle <= 135)
        {
            //Up swipe
            SwipeUpMove();
        }
        else if (swipeAngle > 135 || swipeAngle <= -135)
        {
            //Left swipe
            SwipeLeftMove();
        }
        else if (swipeAngle < -45 && swipeAngle >= -135)
        {
            //Down swipe
            SwipeDownMove();
        }
    }
    void SwipeRightMove()
    {
        //Menukar posisi tile dengan sebelah kanan nya
        otherTile = grid.tiles[column + 1, row];
        otherTile.GetComponent<Tile>().column -= 1;
        column += 1;
    }
    void SwipeUpMove()
    {
        //Menukar posisi tile dengan sebelah atasnya
        otherTile = grid.tiles[column, row + 1];
        otherTile.GetComponent<Tile>().row -= 1;
        row += 1;
    }
    void SwipeLeftMove()
    {
        //Menukar posisi tile dengan sebelah kiri nya
        otherTile = grid.tiles[column - 1, row];
        otherTile.GetComponent<Tile>().column += 1;
        column -= 1;
    }
    void SwipeDownMove()
    {
        //Menukar posisi tile dengan sebelah bawahnya
        otherTile = grid.tiles[column, row - 1];
        otherTile.GetComponent<Tile>().row += 1;
        row -= 1;
    }
    void SwipeTile()
    {
        if (Mathf.Abs(xPosition - transform.position.x) > .1)
        {
            //Move towards the target Kanan/Kiri
            tempPosition = new Vector2(xPosition, transform.position.y);
            transform.position = Vector2.Lerp(transform.position, tempPosition, .4f);
            StartCoroutine(checkMove());
        }
        else
        {
            //Directly set the position
            tempPosition = new Vector2(xPosition, transform.position.y);
            transform.position = tempPosition;
            if (this.gameObject == null)
                Debug.Log("null");

            grid.tiles[column, row] = this.gameObject;
        }

        if (Mathf.Abs(yPosition - transform.position.y) > .1)
        {
            //Move towards the target Atas/Bawah
            tempPosition = new Vector2(transform.position.x, yPosition);
            transform.position = Vector2.Lerp(transform.position, tempPosition, .4f);
            StartCoroutine(checkMove());
        }
        else
        {
            //Directly set the position
            tempPosition = new Vector2(transform.position.x, yPosition);
            transform.position = tempPosition;
            grid.tiles[column, row] = this.gameObject;
        }
    }
    void CheckMatches()
    {
        //Check horizontal matching
        if (column > 0 && column < grid.gridSizeX - 1)
        {
            //Check samping kiri dan kanan nya
            GameObject leftTile = grid.tiles[column - 1, row];
            GameObject rightTile = grid.tiles[column + 1, row];
            if (leftTile != null && rightTile != null)
            {
                if (leftTile.CompareTag(gameObject.tag) && rightTile.CompareTag(gameObject.tag) && Mathf.Abs(xPosition - transform.position.x) < .4)
                {
                    isMatched = true;
                    rightTile.GetComponent<Tile>().isMatched = true;
                    leftTile.GetComponent<Tile>().isMatched = true;
                    if (powerUp || rightTile.GetComponent<Tile>().powerUp || leftTile.GetComponent<Tile>().powerUp)
                    {
                        powerUp = true;
                        rightTile.GetComponent<Tile>().powerUp = true;
                        leftTile.GetComponent<Tile>().powerUp = true;
                    }
                }
            }
        }
        //Check vertical matching
        if (row > 0 && row < grid.gridSizeY - 1)
        {
            //Check samping atas dan bawahnya
            GameObject upTile = grid.tiles[column, row + 1];
            GameObject downTile = grid.tiles[column, row - 1];

            if (upTile != null && downTile != null)
            {
                if (upTile.CompareTag(gameObject.tag) && downTile.CompareTag(gameObject.tag) && Mathf.Abs(yPosition - transform.position.y) < .4)
                {
                    isMatched = true;
                    downTile.GetComponent<Tile>().isMatched = true;
                    upTile.GetComponent<Tile>().isMatched = true;
                    if (powerUp || upTile.GetComponent<Tile>().powerUp || downTile.GetComponent<Tile>().powerUp)
                    {
                        powerUp = true;
                        upTile.GetComponent<Tile>().powerUp = true;
                        downTile.GetComponent<Tile>().powerUp = true;
                    }
                }
            }
        }
        if (isMatched)
        {
            SpriteRenderer sprite = GetComponent<SpriteRenderer>();
            sprite.color = Color.gray;
        }

    }
    IEnumerator checkMove()
    {
        yield return new WaitForSeconds(.5f);
        //Cek jika tile nya tidak sama kembalikan, jika ada yang sama panggil DestroyMatches
        if (otherTile != null)
        {
            if (!isMatched && !otherTile.GetComponent<Tile>().isMatched)
            {
                otherTile.GetComponent<Tile>().row = row;
                otherTile.GetComponent<Tile>().column = column;
                row = previousRow;
                column = previousColumn;
            }
            else
            {
                grid.DestroyMatches(powerUp, gameObject.tag);
            }
        }
        otherTile = null;
    }
}
