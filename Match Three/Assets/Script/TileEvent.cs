﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class TileEvent
{
    //Abstract class untuk base event dari tile

    //Apa yang terjadi jika tile match
    public abstract void OnMatch();
    //Check jika persyaratn event telah terpenuhi
    public abstract bool AchievementCompleted();
}

public class CookiesTileEvent : TileEvent
{
    private int v;

    public CookiesTileEvent(int v)
    {
        this.v = v;
    }

    public override bool AchievementCompleted()
    {
        throw new System.NotImplementedException(); 
    }
    public override bool Equals(object other)
    {
        return base.Equals(other);
    }
    public override int GetHashCode()
    {
        return base.GetHashCode();
    }
    public override void OnMatch()
    {
        throw new System.NotImplementedException();
    }
    public override string ToString()
    {
        return base.ToString();
    }
}

public class CakeTileEvent : TileEvent
{
    private int v;

    public CakeTileEvent(int v)
    {
        this.v = v;
    }

    public override bool AchievementCompleted()
    {
        throw new System.NotImplementedException();
    }
    public override bool Equals(object other)
    {
        return base.Equals(other);
    }
    public override int GetHashCode()
    {
        return base.GetHashCode();
    }
    public override void OnMatch()
    {
        throw new System.NotImplementedException();
    }
    public override string ToString()
    {
        return base.ToString();
    }
}

public class CandyTileEvent : TileEvent
{
    private int v;

    public CandyTileEvent(int v)
    {
        this.v = v;
    }

    public override bool AchievementCompleted()
    {
        throw new System.NotImplementedException();
    }
    public override bool Equals(object other)
    {
        return base.Equals(other);
    }
    public override int GetHashCode()
    {
        return base.GetHashCode();
    }
    public override void OnMatch()
    {
        throw new System.NotImplementedException();
    }
    public override string ToString()
    {
        return base.ToString();
    }
}