﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    // Pemain 1
    public PlayerControl player1; // skrip
    private Rigidbody2D player1Rigidbody;
    private Collider2D player1Collider;

    // Pemain 2
    public PlayerControl player2; // skrip
    private Rigidbody2D player2Rigidbody;
    private Collider2D player2Collider;

    // Bola
    public BallControl ball; // skrip
    private Rigidbody2D ballRigidbody;
    private CircleCollider2D ballCollider;

    public GameObject reverse;


    float impulsePlayer1Y;
    float impulsePlayer2Y;
    public bool lastContactPlayer1;
    private KeyCode oldP1UpButton;
    private KeyCode oldP1DownButton;
    private KeyCode oldP2UpButton;
    private KeyCode oldP2DownButton;

    private float timer;

    // Skor maksimal
    public int maxScore;

    // Apakah debug window ditampilkan?
    private bool isDebugWindowShown = false;

    // Objek untuk menggambar prediksi lintasan bola
    public Trajectory trajectory;

    // Start is called before the first frame update
    // Inisialisasi rigidbody dan collider
    private void Start()
    {
        player1Rigidbody = player1.GetComponent<Rigidbody2D>();
        player2Rigidbody = player2.GetComponent<Rigidbody2D>();
        player1Collider = player1.GetComponent<Collider2D>();
        player2Collider = player2.GetComponent<Collider2D>();
        ballRigidbody = ball.GetComponent<Rigidbody2D>();
        ballCollider = ball.GetComponent<CircleCollider2D>();
        oldP1UpButton = player1.upButton;
        oldP1DownButton = player1.downButton;
        oldP2UpButton = player2.upButton;
        oldP2DownButton = player2.downButton;
        
        InvokeRepeating("checkNewImpulse", 2f, 0.3f);
    }

    // Untuk menampilkan GUI
    void OnGUI()
    {
        // Tampilkan skor pemain 1 di kiri atas dan pemain 2 di kanan atas
        GUI.Label(new Rect(Screen.width / 2 - 150 - 12, 20, 100, 100), "" + player1.Score);
        GUI.Label(new Rect(Screen.width / 2 + 150 + 12, 20, 100, 100), "" + player2.Score);

        // Toggle nilai debug window ketika pemain mengeklik tombol ini.
        if (GUI.Button(new Rect(Screen.width / 2 - 60, Screen.height - 73, 120, 53), "TOGGLE\nDEBUG INFO"))
        {
            isDebugWindowShown = !isDebugWindowShown;
            trajectory.enabled = !trajectory.enabled;
        }

        // Tombol restart untuk memulai game dari awal
        if (GUI.Button(new Rect(Screen.width / 2 - 36, 35, 72, 53), "RESTART"))
        {
            // Ketika tombol restart ditekan, reset skor kedua pemain...
            player1.ResetScore();
            player2.ResetScore();

            // ...dan restart game.
            ball.SendMessage("RestartGame", 0.5f, SendMessageOptions.RequireReceiver);
        }

        // Jika pemain 1 menang (mencapai skor maksimal), ...
        if (player1.Score == maxScore)
        {
            // ...tampilkan teks "PLAYER ONE WINS" di bagian kiri layar...
            GUI.Label(new Rect(Screen.width / 2 - 150, Screen.height / 2 - 10, 2000, 1000), "PLAYER ONE WINS");

            // ...dan kembalikan bola ke tengah.
            ball.SendMessage("ResetBall", null, SendMessageOptions.RequireReceiver);
        }
        // Sebaliknya, jika pemain 2 menang (mencapai skor maksimal), ...
        else if (player2.Score == maxScore)
        {
            // ...tampilkan teks "PLAYER TWO WINS" di bagian kanan layar... 
            GUI.Label(new Rect(Screen.width / 2 + 30, Screen.height / 2 - 10, 2000, 1000), "PLAYER TWO WINS");

            // ...dan kembalikan bola ke tengah.
            ball.SendMessage("ResetBall", null, SendMessageOptions.RequireReceiver);
        }

        // Jika isDebugWindowShown == true, tampilkan text area untuk debug window.
        if (isDebugWindowShown)
        {
            // Simpan variabel-variabel fisika yang akan ditampilkan. 
            float ballMass = ballRigidbody.mass;
            Vector2 ballVelocity = ballRigidbody.velocity;
            float ballSpeed = ballRigidbody.velocity.magnitude;
            Vector2 ballMomentum = ballMass * ballVelocity;
            float ballFriction = ballCollider.friction;

            float impulsePlayer1X = player1.LastContactPoint.normalImpulse;
            float impulsePlayer1Y = player1.LastContactPoint.tangentImpulse;
            float impulsePlayer2X = player2.LastContactPoint.normalImpulse;
            float impulsePlayer2Y = player2.LastContactPoint.tangentImpulse;

            // Tentukan debug text-nya
            string debugText =
                "Ball mass = " + ballMass + "\n" +
                "Ball velocity = " + ballVelocity + "\n" +
                "Ball speed = " + ballSpeed + "\n" +
                "Ball momentum = " + ballMomentum + "\n" +
                "Ball friction = " + ballFriction + "\n" +
                "Last impulse from player 1 = (" + impulsePlayer1X + ", " + impulsePlayer1Y + ")\n" +
                "Last impulse from player 2 = (" + impulsePlayer2X + ", " + impulsePlayer2Y + ")\n";

            // Simpan nilai warna lama GUI
            Color oldColor = GUI.backgroundColor;

            // Beri warna baru
            GUI.backgroundColor = Color.red;

            // Tampilkan debug window
            GUIStyle guiStyle = new GUIStyle(GUI.skin.textArea);
            guiStyle.alignment = TextAnchor.UpperCenter;
            GUI.TextArea(new Rect(Screen.width / 2 - 200, Screen.height - 200, 400, 110), debugText, guiStyle);

            // Kembalikan warna lama GUI
            GUI.backgroundColor = oldColor;
        }
    }

    // Update is called once per frame
    void Update()
    {
        timer += Time.deltaTime;
        float newImpulsePlayer1Y = player1.LastContactPoint.tangentImpulse;
        float newImpulsePlayer2Y = player2.LastContactPoint.tangentImpulse;
        
        if (impulsePlayer1Y != newImpulsePlayer1Y)
        { 
            lastContactPlayer1 = true;
            impulsePlayer1Y = newImpulsePlayer1Y;
        }
        if (impulsePlayer2Y != newImpulsePlayer2Y)
        {
            lastContactPlayer1 = false;
            impulsePlayer2Y = newImpulsePlayer2Y;
        }
        if (ball.powerUpActive && ball.powerUpPlayer == 1)
        {
            player1.upButton = oldP1DownButton;
            player1.downButton = oldP1UpButton;
            SpriteRenderer sprite = player1.GetComponent<SpriteRenderer>();
            sprite.color = Color.blue;
        }
        else if (ball.powerUpActive && ball.powerUpPlayer == 2)
        {
            player2.upButton = oldP2DownButton;
            player2.downButton = oldP2UpButton;
            SpriteRenderer sprite = player2.GetComponent<SpriteRenderer>();
            sprite.color = Color.blue;
        }
        if (!ball.powerUpActive)
        {
            player1.upButton = oldP1UpButton;
            player1.downButton = oldP1DownButton;
            player2.upButton = oldP2UpButton;
            player2.downButton = oldP2DownButton;
            SpriteRenderer sprite1 = player1.GetComponent<SpriteRenderer>();
            SpriteRenderer sprite2 = player2.GetComponent<SpriteRenderer>();
            sprite1.color = Color.white;
            sprite2.color = Color.white;
        }

        if (ball.ballPhase)
        {
            player1Collider.enabled = false;
            player2Collider.enabled = false;
        }
        else
        {
            player1Collider.enabled = true;
            player2Collider.enabled = true;
        }
        if (timer > 6f)
        {
            Vector2 position = new Vector2(Random.Range(-12f, 12f), Random.Range(-8f, 8f));
            Instantiate(reverse, position, Quaternion.identity);
            timer = timer - 6f;
        }
    }


    void checkNewImpulse()
    {
        impulsePlayer1Y = player1.LastContactPoint.tangentImpulse;
        impulsePlayer2Y = player2.LastContactPoint.tangentImpulse;
    }
}
