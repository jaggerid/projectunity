﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerUp : MonoBehaviour
{
    float timer;
    [SerializeField]
    public string powerUpName;

    private void Update()
    {
        timer += Time.deltaTime;
        if (timer > 6f)
        {
            Destroy(gameObject);
        }
    }
}
