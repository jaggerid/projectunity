﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class BallControl : MonoBehaviour
{

    // Rigidbody 2D bola
    private Rigidbody2D rigidBody2D;

    // Besarnya gaya awal yang diberikan untuk mendorong bola
    public float xInitialForce;
    public float yInitialForce;
    int speed = 2;

    // Titik asal lintasan bola saat ini
    private Vector2 trajectoryOrigin;

    private float timer;
    private float gameTime;
    private float phaseTime;
    private PowerUp power;
    public bool powerUpActive, ballPhase;
    public int powerUpPlayer;
    public GameManager gm;

    void Start()
    {
        trajectoryOrigin = transform.position;
        rigidBody2D = GetComponent<Rigidbody2D>();
        gm = gm.GetComponent<GameManager>();

        RestartGame();
    }

    void Update()
    {
        timer += Time.deltaTime;
        gameTime += Time.deltaTime;
        phaseTime += Time.deltaTime;

        if (powerUpActive && timer > 5f)
        {
            powerUpActive = false;
        }

        if (gameTime >= 20)
        {
            float chance = Random.Range(0f, 100f);
            if (chance <= 1)
            {
                phaseTime = 0;
                ballPhase = true;
                SpriteRenderer sprite = GetComponent<SpriteRenderer>();
                sprite.color = Color.white;
            }
            if (ballPhase && phaseTime > 1)
            { 
                ballPhase = false;
                phaseTime = 0;
                SpriteRenderer sprite = gameObject.GetComponent<SpriteRenderer>();
                sprite.color = Color.red;
            }
        }
    }

    void ResetBall()
    {
        // Reset posisi menjadi (0,0)
        transform.position = Vector2.zero;

        // Reset kecepatan menjadi (0,0)
        rigidBody2D.velocity = Vector2.zero;

        //RESET time
        SpriteRenderer sprite = gameObject.GetComponent<SpriteRenderer>();
        sprite.color = Color.red;
        gameTime = 0;
        ballPhase = false;
    }

    void PushBall()
    {
        // Tentukan nilai komponen y dari gaya dorong antara -yInitialForce dan yInitialForce
        float yRandomInitialForce = Random.Range(-yInitialForce, yInitialForce);

        // Tentukan nilai acak antara 0 (inklusif) dan 2 (eksklusif)
        float randomDirection = Random.Range(0, 2);
        
        float magnitude = Mathf.Sqrt((xInitialForce * xInitialForce) + (yInitialForce * yInitialForce));
        float xAfter = Mathf.Sqrt((magnitude * magnitude) - (yRandomInitialForce * yRandomInitialForce));

        // Jika nilainya di bawah 1, bola bergerak ke kiri. 
        // Jika tidak, bola bergerak ke kanan.
        if (randomDirection < 1.0f)
        {
            // Gunakan gaya untuk menggerakkan bola ini.
            rigidBody2D.AddForce(new Vector2(-xAfter, yRandomInitialForce));
        }
        else
        {
            rigidBody2D.AddForce(new Vector2(xAfter, yRandomInitialForce));
        }
        
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (Mathf.Sign(rigidBody2D.velocity.y) == -1)
        {
            Vector2 velocityTweak = new Vector2(rigidBody2D.velocity.x, rigidBody2D.velocity.y + Random.Range(-1f, -3f));
            rigidBody2D.velocity = velocityTweak;
        }
        else if (Mathf.Sign(rigidBody2D.velocity.y) == 1)
        {
            Vector2 velocityTweak = new Vector2(rigidBody2D.velocity.x, rigidBody2D.velocity.y + Random.Range(1f, 3f));
            rigidBody2D.velocity = velocityTweak;
        }
    }
    void OnTriggerEnter2D(Collider2D collider)
    {
        power = collider.GetComponent<PowerUp>();
        if (power != null)
        {
            if (power.powerUpName == "Reverse")
            {
                if (gm.lastContactPlayer1)
                {
                    powerUpPlayer = 1;
                }
                else
                {
                    powerUpPlayer = 2;
                }
                powerUpActive = true;
                timer = 0f;
                Destroy(collider.gameObject);
            }
        }
    }
    // Ketika bola beranjak dari sebuah tumbukan, rekam titik tumbukan tersebut
    private void OnCollisionExit2D(Collision2D collision)
    {
        trajectoryOrigin = transform.position;
    }

    // Untuk mengakses informasi titik asal lintasan
    public Vector2 TrajectoryOrigin
    {
        get { return trajectoryOrigin; }
    }

    void RestartGame()
    {
        // Kembalikan bola ke posisi semula
        ResetBall();

        // Setelah 2 detik, berikan gaya ke bola
        Invoke("PushBall", 2);

    }
}
