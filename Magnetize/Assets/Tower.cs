﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tower : MonoBehaviour
{
    PlayerController player;
    public static bool towerPulled = false;

    // Start is called before the first frame update
    private void Start()
    {
        player = GameObject.Find("Player").GetComponent<PlayerController>();
    }
    private void OnMouseDown()
    {
        float distance = Vector2.Distance(player.transform.position, transform.position);

        //Gravitation toward tower
        Vector3 pullDirection = (gameObject.transform.position - transform.position).normalized;
        float newPullForce = Mathf.Clamp(player.pullForce / distance, 30, 100);
        player.rb2D.AddForce(pullDirection * newPullForce);

        //Angular velocity
        player.rb2D.angularVelocity = - player.rotateSpeed / distance;

        player.isPulled = true;
        towerPulled = true;
        gameObject.GetComponent<SpriteRenderer>().color = Color.green;
    }
    private void OnMouseUp()
    {
        gameObject.GetComponent<SpriteRenderer>().color = Color.white;
        player.rb2D.angularVelocity = 0;
        towerPulled = false;
    }
}
