﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerController : MonoBehaviour
{
    public Rigidbody2D rb2D;
    public float moveSpeed = 5f;
    public float pullForce = 100f;
    public float rotateSpeed = 360f;
    private GameObject hookedTower;
    private GameObject closestTower;
    Vector3 mousePosition;
    public bool isPulled = false;
    private UIControllerScript uiControl;
    private AudioSource myAudio;
    private bool isCrashed = false;


    // Start is called before the first frame update
    void Start()
    {
        rb2D = this.gameObject.GetComponent<Rigidbody2D>();
        uiControl = GameObject.Find("Canvas").GetComponent<UIControllerScript>();
        myAudio = this.gameObject.GetComponent<AudioSource>();

            
    }

    // Update is called once per frame
    void Update()
    {
        //Move the object
        if ((Input.GetKey(KeyCode.Z) || Input.GetMouseButtonDown(0)) && !isPulled && !Tower.towerPulled)
        {
            if (closestTower != null && hookedTower == null)
            {
                hookedTower = closestTower;
            }
            if (hookedTower)
            {
                float distance = Vector2.Distance(transform.position, hookedTower.transform.position);

                //Gravitation toward tower
                Vector3 pullDirection = (hookedTower.transform.position - transform.position).normalized;
                float newPullForce = Mathf.Clamp(pullForce / distance, 30, 100);
                rb2D.AddForce(pullDirection * newPullForce);

                //Angular velocity
                rb2D.angularVelocity = -rotateSpeed / distance;

                isPulled = true;
            }
        }

        if ((Input.GetKeyUp(KeyCode.Z) || Input.GetMouseButtonUp(0)) && (!isCrashed))
        {
            isPulled = false;
            rb2D.angularVelocity = 0;
        }

        if (!myAudio.isPlaying && isCrashed)
        {
            RestartPosition();
        }
        else
        {
            rb2D.velocity = -transform.up * moveSpeed;
        }
    }

    public void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Goal")
        {
            Debug.Log("Level Clear!");
            uiControl.endGame(true);
        }
    }

    public void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Wall")
        {
            isCrashed = true;
            myAudio.Play();
            if(myAudio.isPlaying) { Debug.Log("crash"); }
            if (closestTower)
            {
                closestTower.gameObject.GetComponent<SpriteRenderer>().color = Color.white;
                closestTower = null;
                hookedTower = null;
            }
            //uiControl.endGame(false);

            
        }
    }

    public void RestartPosition()
    {
        rb2D.angularVelocity = 0;
        transform.rotation = Quaternion.Euler(0, 0, 90f);
        transform.position = new Vector2(-7.04f, 2.91f);
        isCrashed = false;
        //rb2D.velocity = -transform.up * moveSpeed;
    }

    public void OnTriggerStay2D(Collider2D collision)
    {
        if (Tower.towerPulled)
        {
            collision.gameObject.GetComponent<SpriteRenderer>().color = Color.white;
            return;
        }

        if (collision.gameObject.tag == "Tower")
        {
            closestTower = collision.gameObject;
            //Change tower color back to green as indicator
            collision.gameObject.GetComponent<SpriteRenderer>().color = Color.green;
        }
    }

    public void OnTriggerExit2D(Collider2D collision)
    {
        if (isPulled) return;

        if (collision.gameObject.tag == "Tower")
        {
            closestTower = null;

            //Change tower color back to normal
            collision.gameObject.GetComponent<SpriteRenderer>().color = Color.white;
        }
    }
}
