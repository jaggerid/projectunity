﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    public static int round, remaining;
    public Text textRemaining, textRound;
    Animator anim;

    // Start is called before the first frame update
    void Start()
    {
        round = 1;
        remaining = round * 10;
        anim = GetComponent<Animator>();
        textRemaining = GameObject.Find("/HUDCanvas/EnemyRemaining").GetComponent<Text>();
        textRound = GameObject.Find("/HUDCanvas/Round").GetComponent<Text>();
    }

    // Update is called once per frame
    void Update()
    {
        if (remaining == 0)
        {
            round++;
            EnemyManager.spawn = 0;
            anim.SetTrigger("RoundClear");
            remaining = round * 10;
        }
        
        textRemaining.text = "Enemy: " + remaining;
        textRound.text = "Round: " + round;
        
    }
}
