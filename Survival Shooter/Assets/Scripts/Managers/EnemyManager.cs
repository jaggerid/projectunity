﻿using UnityEngine;

public class EnemyManager : MonoBehaviour
{
    public static bool isSpawn = true;
    public PlayerHealth playerHealth;
    public GameObject enemy;
    public float spawnTime = 3f;
    public static int spawn = 0;
    public Transform[] spawnPoints;

    [SerializeField]
    MonoBehaviour factory;
    IFactory Factory { get { return factory as IFactory; } }

    void Start()
    {
        //Mengeksekusi fungs Spawn setiap beberapa detik sesui dengan nilai spawnTime
        
        InvokeRepeating("Spawn", spawnTime, spawnTime);
            
    }

    void Spawn()
    {
        //Jika player telah mati maka tidak membuat enemy baru
        if (playerHealth.currentHealth <= 0f)
        {
            return;
        }
        if (spawn >= GameManager.round * 10)
        {
            return;
        }
        if (isSpawn)
        {
            //Mendapatkan nilai random
            int spawnPointIndex = Random.Range(0, spawnPoints.Length);
            int spawnEnemy = Random.Range(0, 3);

            //Memduplikasi enemy
            Factory.FactoryMethod(spawnEnemy, spawnPoints[spawnPointIndex].position, spawnPoints[spawnPointIndex].rotation);

            //Instantiate(enemy, spawnPoints[spawnPointIndex].position, spawnPoints[spawnPointIndex].rotation);
            spawn++;
        }
    }
}