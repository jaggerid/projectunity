﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyFactory : MonoBehaviour, IFactory
{

    [SerializeField]
    public GameObject[] enemyPrefab;

    public void Start()
    {
        
    }
    public GameObject FactoryMethod(int tag, Vector3 spawnPosition, Quaternion spawnRotation)
    {
        GameObject enemy = Instantiate(enemyPrefab[tag], spawnPosition, spawnRotation);
        return enemy;
    }
}