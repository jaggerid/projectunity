﻿using UnityEngine;

public class PlayerShooting : MonoBehaviour
{
    public float shotgunAtkTime = 2f;
    public static int damagePerShot = 10;
    public float timeBetweenBullets = 0.15f;
    public float range = 100f;


    float shotgunTimer, timer;
    Ray shootRay = new Ray();
    RaycastHit shootHit;
    int shootableMask;
    ParticleSystem gunParticles;
    LineRenderer gunLine;
    AudioSource gunAudio;
    Light gunLight;
    Color oldLight, oldLine;
    float effectsDisplayTime = 0.2f;


    void Awake ()
    {
        shootableMask = LayerMask.GetMask ("Shootable");
        gunParticles = GetComponent<ParticleSystem> ();
        gunLine = GetComponent <LineRenderer> ();
        gunAudio = GetComponent<AudioSource> ();
        gunLight = GetComponent<Light> ();
        oldLight = gunLight.color;
        oldLine = gunLine.material.color;
    }


    void Update ()
    {
        timer += Time.deltaTime;
        shotgunTimer += Time.deltaTime;

		if(Input.GetButton ("Fire1") && timer >= timeBetweenBullets && Time.timeScale != 0)
        {
            Shoot(damagePerShot, range);
            gunLight.color = oldLight;
            gunLine.material.color = oldLine;
        }
        else if (Input.GetButton("Fire2") && shotgunTimer >= shotgunAtkTime && Time.timeScale != 0 && !Input.GetButton("Fire1"))
        {
            Shoot(damagePerShot * 10, range / 5);
            gunLight.color = Color.red;
            gunLine.material.color = Color.red;
            shotgunTimer = 0f;
        }
        if (timer >= timeBetweenBullets * effectsDisplayTime)
        {
            DisableEffects ();
        }
    }


    public void DisableEffects ()
    {
        gunLine.enabled = false;
        gunLight.enabled = false;
    }


    public void Shoot (int damagePerShot = 20, float range = 100f)
    {
        timer = 0f;

        gunAudio.Play ();

        gunLight.enabled = true;

        gunParticles.Stop ();
        gunParticles.Play ();

        gunLine.enabled = true;
        gunLine.SetPosition (0, transform.position);

        shootRay.origin = transform.position;
        shootRay.direction = transform.forward;

        if(Physics.Raycast (shootRay, out shootHit, range, shootableMask))
        {
            EnemyHealth enemyHealth = shootHit.collider.GetComponent <EnemyHealth> ();
            if(enemyHealth != null)
            {
                enemyHealth.TakeDamage (damagePerShot, shootHit.point);
            }
            gunLine.SetPosition (1, shootHit.point);
        }
        else
        {
            gunLine.SetPosition (1, shootRay.origin + shootRay.direction * range);
        }
    }
}
