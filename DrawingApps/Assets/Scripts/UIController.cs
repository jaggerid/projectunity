﻿using UnityEngine;
using UnityEngine.UI;

public class UIController : MonoBehaviour
{

    public AffineTransformer affineTransformer;
    public Painter painter;

    // input matriks transformasi
    public InputField Mat_A;
    public InputField Mat_B;
    public InputField Mat_C;
    public InputField Mat_D;
    public InputField InputSegi;

    [SerializeField] InputField DegreeInput;
    [SerializeField] Button TransformButton;
    [SerializeField] Button ColorPickerButton;

    // Start is called before the first frame update
    void Start()
    {
        
        TransformButton.onClick.AddListener(TransformButton_OnClick);
        ColorPickerButton.onClick.AddListener(ColorPickerButton_OnClick);
        DegreeInput.onValueChanged.AddListener(DegreeInput_OnEndEdit);
        InputSegi.onValueChanged.AddListener(Segi_OnEndEdit);
    }

    public void ColorPickerButton_OnClick()
    {
        GameObject gameObject = this.transform.Find("ColorPicker").gameObject;
        if(gameObject.activeSelf)
        {
            gameObject.SetActive(false);
        }
        else
        {
            gameObject.SetActive(true);
        }
    }
    public void TransformButton_OnClick()
    {
        affineTransformer.ExecuteAffineTransformation(float.Parse(Mat_A.text), float.Parse(Mat_B.text), float.Parse(Mat_C.text), float.Parse(Mat_D.text));
    }

    void Segi_OnEndEdit(string value)
    {
        painter.segi = int.Parse(InputSegi.text);
    }
    void DegreeInput_OnEndEdit(string value)
    {
        if (!string.IsNullOrEmpty(value))
        {
            float degree = float.Parse(value);

            float a = Mathf.Cos(degree * Mathf.Deg2Rad);
            float b = Mathf.Sin(degree * Mathf.Deg2Rad);
            float c = -Mathf.Sin(degree * Mathf.Deg2Rad);
            float d = Mathf.Cos(degree * Mathf.Deg2Rad);

            Mat_A.text = a.ToString();
            Mat_B.text = b.ToString();
            Mat_C.text = c.ToString();
            Mat_D.text = d.ToString();
        }
    }
}